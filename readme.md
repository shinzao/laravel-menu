# This project was replaced by [enso/helper](https://gitlab.com/shinzao/laravel-helper)
## Please use the new package, which offers the same functionality combined with some more simple helpers for laravel and bootstrap.
------------------------------------------------
------------------------------------------------

# Enso Menu
Simple laravel helper facade to set active menu state based on current route.

### Requirements
    Laravel >= 5.2
    PHP >= 7.0

## Installation

1. Add the requirements with composer
```
composer require enso/menu
```

2. Add the service provider to your `/config/app.php` file.
```php
'providers' => [
   ...
   Enso\Menu\MenuServiceProvider::class,
],
```
    
3. Add the facade alias to your `/config/app.php` file.
```php
'aliases' => [
   ...    
   'Menu' => Enso\Menu\Facades\MenuFacade::class,
],
```

## Usage

Use the `Menu::active($routes, $onlyClass)` method within your blade templates where you want to add the `active` css class.

- Set the `$routes` parameter as to any route you want to check, or an array of multiple routes. You can also use the `*` wilcard.
- Set the `$onlyClass` parameter to true, if you only want to get the string "active" instead of the whole class markup.

Example:
```
<li{{ Menu::active(['profile', 'profile/*']) }}>
    <a href="{{ url('profile') }}">Your profile</a>
</li>
```

## Author

Daniel Hoffmann | [Enso](http://enso.re)