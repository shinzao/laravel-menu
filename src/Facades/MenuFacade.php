<?php
namespace Enso\Menu\Facades;

use Illuminate\Support\Facades\Facade;

class MenuFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'menu';
    }
}