<?php
namespace Enso\Menu\Http;

use Illuminate\Contracts\View\Factory;
use Request;

class MenuController
{
    protected $view;

    /**
     * MenuController constructor.
     * @param Factory $view
     */
    public function __construct(Factory $view)
    {
        $this->view = $view;
    }

    /**
     * @param $routes
     * @param bool $onlyClass
     */
    public function active($routes, $onlyClass = false)
    {
        if (!is_array($routes)) {
            $routes = [$routes];
        }

        foreach ($routes as $route) {
            if (Request::is($route)) {
                if ($onlyClass) {
                    echo ' active';
                } else {
                    echo ' class="active"';
                }
                break;
            }
        }
    }
}
