<?php

namespace Enso\Menu;

use Illuminate\Support\ServiceProvider;
use Enso\Menu\Http\MenuController;

class MenuServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMenuController();

        $this->app->alias('menu', 'Enso\Menu\Http\MenuController');
    }

    protected function registerMenuController()
    {
        $this->app->singleton('menu', function ($app) {
            return new MenuController($app['view']);
        });
    }

    public function provides()
    {
        return ['menu', 'Enso\Menu\Http\MenuController'];
    }
}
